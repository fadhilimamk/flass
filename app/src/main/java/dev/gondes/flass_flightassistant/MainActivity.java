package dev.gondes.flass_flightassistant;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dev.gondes.flass_flightassistant.adapter.MainPagerAdapter;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.timerHour) TextView timerHour;
    @BindView(R.id.timerMinute) TextView timerMinute;
    @BindView(R.id.txtHour) TextView txtHour;
    @BindView(R.id.txtMinute) TextView txtMinute;
    @BindView(R.id.separator) TextView txtSeparator;
    @BindView(R.id.txtMessage) TextView txtMessage;
    @BindView(R.id.txtTimerMessage) TextView txtTimerMessage;
    @BindView(R.id.timerProgressBar) ProgressBar timerProgresBar;
    @BindView(R.id.mainPager) ViewPager mainPager;

    final long interval = 1000L; // 1 minute interval for timer

    private long currentTime = new Date().getTime();
    private long targetTime;
    private long timeDiff;

    private int currentTimerHour = 10;
    private int currentTimerMinute = 59;

    private MainPagerAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        // Initializing pager
        pagerAdapter = new MainPagerAdapter(getSupportFragmentManager());
        mainPager.setAdapter(pagerAdapter);
        showPromoFragment();
        final View touchView = findViewById(R.id.mainPager);
        touchView.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });

        // get target time from api
        targetTime = currentTime + 180000L;

        timeDiff = targetTime - currentTime;
        currentTimerHour = (int) (timeDiff / 60000L);
        currentTimerMinute = (int) (timeDiff%60000L);

        txtHour.setVisibility(View.VISIBLE);
        txtMinute.setVisibility(View.VISIBLE);

        timerProgresBar.setMax(100);
        timerProgresBar.setProgress(1);
        runTimer();

    }

    private void runTimer() {
        new CountDownTimer(targetTime - currentTime, interval) {

            private long tick;

            @Override
            public void onTick(long l) {

                tick++;

                currentTimerMinute--;
                if (currentTimerMinute == -1) {
                    currentTimerHour--;
                    currentTimerMinute = 59;
                }

                String minuteZero = (currentTimerMinute < 10) ? "0" : "";
                String hourZero = (currentTimerHour < 10) ? "0" : "";

                timerHour.setText(hourZero + Integer.toString(currentTimerHour));
                timerMinute.setText(minuteZero + Integer.toString(currentTimerMinute));

                if (timerProgresBar.getProgress() != timerProgresBar.getMax()) {
                    double percent = (double) (tick*interval) / (double) timeDiff;
                    int progress = (int)(percent * 100);
                    timerProgresBar.setProgress(progress);
                }

            }

            @Override
            public void onFinish() {
                Toast.makeText(getApplicationContext(), "Have a nice flight", Toast.LENGTH_LONG).show();
                timerProgresBar.setProgress(100);
                hideTimer();
                txtTimerMessage.setText("pesawat telah berangkat");
                txtMessage.setText("Have a nice flight!");
                txtMessage.setVisibility(View.VISIBLE);
            }

        }.start();
    }

    private void hideTimer() {
        timerHour.setVisibility(View.INVISIBLE);
        timerMinute.setVisibility(View.INVISIBLE);
        txtHour.setVisibility(View.INVISIBLE);
        txtMinute.setVisibility(View.INVISIBLE);
        txtSeparator.setVisibility(View.INVISIBLE);
    }

    private void showTimer() {
        timerHour.setVisibility(View.VISIBLE);
        timerMinute.setVisibility(View.VISIBLE);
        txtHour.setVisibility(View.VISIBLE);
        txtMinute.setVisibility(View.VISIBLE);
        txtSeparator.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.btnContext)
    public void showContext() {
        Intent intent = new Intent(this, VideoActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btnMenuPromo)
    public void showPromoFragment() {
        mainPager.setCurrentItem(0);
    }

    @OnClick(R.id.btnMenuBaggage)
    public void showBaggageFragment() {
        mainPager.setCurrentItem(1);
    }

}
