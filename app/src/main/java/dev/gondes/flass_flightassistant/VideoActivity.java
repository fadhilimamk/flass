package dev.gondes.flass_flightassistant;

import android.content.res.TypedArray;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

import dev.gondes.flass_flightassistant.adapter.VideoAdapter;
import dev.gondes.flass_flightassistant.model.Video;

public class VideoActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private ArrayList<Video> mVideoData;
    private VideoAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mVideoData = new ArrayList<>();

        mAdapter = new VideoAdapter(this, mVideoData);
        mRecyclerView.setAdapter(mAdapter);

        initializeData();

    }

    private void initializeData() {

        String[] videoList = getResources().getStringArray(R.array.video_titles);
        String[] urlList = getResources().getStringArray(R.array.video_url);

        mVideoData.clear();

        for (int i = 0; i < videoList.length; i++) {
            mVideoData.add(new Video(videoList[i],urlList[i],1));
        }
    }

}
